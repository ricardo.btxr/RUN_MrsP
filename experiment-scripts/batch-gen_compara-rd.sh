#!/bin/bash

rw=0.15   
rn=20
ut=7
ncpu=8
ne=100
echo "to_csv,num_tasks,res_distr,orig_util,SBLP_util,SBLP_inflation,MrsP_util,MrsP_inflation,Delta(SBLP/MrsP)" > \
	compara-rd-rw=${rw}_rn=${rn}_ut=${ut}.csv

for rd in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1
do

   ./gen_exps.py \
	-fr RUN_Comparativo max_util=${ut} -n${ne} \
	cpus=${ncpu} res_distr=${rd} res_weight=${rw} res_nmb=${rn} \
        -o exps/compara_ut=${ut}_rd=${rd}_rw=${rw}_rn=${rn} | \
	grep "^to_csv" >> compara-rd-rw=${rw}_rn=${rn}_ut=${ut}.csv

done
