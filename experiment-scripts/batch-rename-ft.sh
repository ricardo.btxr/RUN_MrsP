#!/bin/bash

base_dir=run-mrsp

for exp_dir in $(ls run-mrsp/)
do

  for i in {0..99}
  do

    cd ${base_dir}/${exp_dir}/sched_RUN_trial_${i}/
    mv overhead0_ft.bin ft.bin
    cd ../../../

  done
done

