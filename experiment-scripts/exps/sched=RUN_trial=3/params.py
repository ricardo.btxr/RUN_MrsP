{'cpus': 4,
 'duration': 30,
 'final_util': '3.540000',
 'max_util': '3.4',
 'periods': 'harmonic',
 'release_master': False,
 'res_distr': '0.8',
 'res_nmb': '6',
 'res_weight': '0.15',
 'scheduler': 'RUN',
 'trial': 3,
 'utils': 'uni-medium'}
