#!/bin/bash

base_dir=exps/004

for exp_dir in $(ls exps/004/)
do
  echo "========================================================"
  echo "Processando ${exp_dir}"
  echo "========================================================"

  ./run_exps.py -f ${base_dir}/${exp_dir}/sched=RUN_trial=0 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=1 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=2 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=3 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=4 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=5 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=6 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=7 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=8 \
  	 ${base_dir}/${exp_dir}/sched=RUN_trial=9 \
	 -o run-data/${exp_dir}

done

echo "========================================================"
echo "Final dos experimentos"
echo "========================================================"

