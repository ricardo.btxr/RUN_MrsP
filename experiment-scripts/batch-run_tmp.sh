#!/bin/bash

#base_dir="exps-sblp"

#exp_dir="sblp_ut=3.5_rd=1_rw=0.04_rn=4_u=0.075-0.325_p=harmonic-2"
#./run_exps.py -f ${base_dir}/${exp_dir}/sched=RUN_trial=48 -o run-sblp/${exp_dir}

#exp_dir="sblp_ut=3.5_rd=1_rw=0.06_rn=4_u=0.075-0.325_p=harmonic-2"
#./run_exps.py -f ${base_dir}/${exp_dir}/sched=RUN_trial=48 -o run-sblp/${exp_dir}

base_dir="exps-mrsp"
exp_dir="mrsp_ut=3.5_rd=0.8_rw=0.04_rn=4_u=0.075-0.325_p=harmonic-2"
./run_exps.py -f ${base_dir}/${exp_dir}/sched=RUN_trial=0 -o run-tmp/${exp_dir}
./run_exps.py -f ${base_dir}/${exp_dir}/sched=RUN_trial=1 -o run-tmp/${exp_dir}
./run_exps.py -f ${base_dir}/${exp_dir}/sched=RUN_trial=2 -o run-tmp/${exp_dir}
./run_exps.py -f ${base_dir}/${exp_dir}/sched=RUN_trial=3 -o run-tmp/${exp_dir}
./run_exps.py -f ${base_dir}/${exp_dir}/sched=RUN_trial=4 -o run-tmp/${exp_dir}
