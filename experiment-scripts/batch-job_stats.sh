#!/bin/bash

base_dir=run-new2

for exp_dir in $(ls run-new2/)
do

  echo "Processando ${exp_dir}"

  for i in {0..99}
  do
    st-job-stats -n rtspin ${base_dir}/${exp_dir}/sched=RUN_trial=${i}/st-*.bin \
	 > ${base_dir}/${exp_dir}/sched=RUN_trial=${i}/stats.txt

    cat ${base_dir}/${exp_dir}/sched=RUN_trial=${i}/stats.txt \
         >> ${base_dir}/stats.txt
  done

done
