#!/bin/bash

for rd in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1
do
    for rw in 0.15   
    do
        for rn in 20
        do
	    ut=7
	    ncpu=8
	    ne=50

            ./gen_exps.py \
                -fr RUN_Comparativo max_util=${ut} -n${ne} \
		cpus=${ncpu} res_distr=${rd} res_weight=${rw} res_nmb=${rn} \
                -o exps/compara_ut=${ut}_rd=${rd}_rw=${rw}_rn=${rn}

        done
    done
done
