#!/bin/bash

base_dir=run-data

for exp_dir in $(ls run-data/) 
do

  echo "Processando ${exp_dir}"

  for i in 0 1 2 3 4 5 6 7 8 9 
  do
    st-job-stats -n rtspin ${base_dir}/${exp_dir}/sched=RUN_trial=${i}/st-*.bin \
	 > ${base_dir}/${exp_dir}/sched=RUN_trial=${i}/stats.txt

    cat ${base_dir}/${exp_dir}/sched=RUN_trial=${i}/stats.txt \
         >> ${base_dir}/stats.txt
  done

done
