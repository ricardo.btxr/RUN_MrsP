SCHEDCAT_PATH="/home/ricardo/litmus/schedcat"
LITMUS_PATH="/home/ricardo/litmus/liblitmus"
FEATHERTRACE_PATH="/home/ricardo/litmus/feather-trace-tools"
AUTOMATE_PATH="/home/ricardo/litmus/automate"
CHEETAH_PATH="/home/ricardo/litmus/cheetahtemplate-cheetah-7b1c2ad"

export PATH=$PATH:$LITMUS_PATH:$FEATHERTRACE_PATH:$AUTOMATE_PATH
export PYTHONPATH=$PYTHONPATH:$SCHEDCAT_PATH:$CHEETAH_PATH

