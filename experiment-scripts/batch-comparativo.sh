#!/bin/bash

#for rd in 0.6 0.8 0.9
for rd in 0.8 0.9 1
do
#    for rw in 0.1 0.15  
#    for rw in 0.2 0.25
    for rw in 0.3 0.4
    do
        for rn in 10 20 40
        do
            ./gen_exps.py \
                -fr RUN_Comparativo max_util=6.5 -n10 cpus=8 res_distr=${rd} res_weight=${rw} res_nmb=${rn} >> \
                run-data/Comparativo/compara_max_util=7_res_distr=${rd}_res_wheight=${rw}_res_nmb=${rn}.txt
        done
    done
done
