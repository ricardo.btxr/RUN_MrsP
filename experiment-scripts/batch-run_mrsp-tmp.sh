#!/bin/bash

base_dir=exps-new2

for exp_dir in $(ls exps-new2/)
do
  echo "========================================================"
  echo "Processando ${exp_dir}"
  echo "========================================================"

  ./run_exps.py \
         ${base_dir}/${exp_dir}/sched=RUN_trial=2 \
	 -o run-data/debug/${exp_dir}_tmp

done

echo "========================================================"
echo "Final dos experimentos"
echo "========================================================"

