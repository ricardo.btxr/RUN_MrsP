#!/bin/bash

base_dir=run-mrsp

for exp_dir in $(ls run-mrsp/)
do

  echo "Processando ${exp_dir}"

  for i in {44..44}
  do
    st-job-stats -n rtspin ${base_dir}/${exp_dir}/sched=RUN_trial=${i}/st-*.bin \
	 > ${base_dir}/${exp_dir}/sched=RUN_trial=${i}/stats.txt
  done

done
