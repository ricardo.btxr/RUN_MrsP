{'cpus': 4,
 'duration': 30,
 'final_util': '3.580333',
 'max_util': '3.5',
 'periods': 'harmonic-2',
 'release_master': False,
 'res_distr': '0.8',
 'res_nmb': '4',
 'res_weight': '0.04',
 'scheduler': 'RUN',
 'trial': 81,
 'utils': 'uni-medium-3'}
